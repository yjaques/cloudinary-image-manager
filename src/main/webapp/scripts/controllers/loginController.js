'use strict';
/**
 * @description controller for login and user profile display, attached to a div in the nav bar.
 *
 * @class loginController
 * @param {Object} $log Angular logging framework
 * @param {Object} $scope Angular view model
 * @param {Object} $rootScope the scope of all scopes
 * @param {Object} $location Angular location wrapper
 * @param {Object} UserService handles user and geo locating services
 * @param {Object} CONSTANTS Constants for the controller
 */
app.controller('loginController', function($log, $scope, $rootScope, $location, UserService, CONSTANTS) {

    $scope.login = function() {
        UserService.getUser();

    };

    $scope.logout = function() {
        UserService.logout();
    };

    //only do this if we need a login and we are not coming in on a redirect as seems to break Google's OAUTH, no idea why
    if (CONSTANTS.REQUIRE_LOGIN && $location.url().indexOf('/index.html?domain=') == -1) {
        $scope.login();
    }

    //only do this if geo location is wanted
    if(CONSTANTS.GEO_LOCATE) {
        UserService.getLocation();
    }

    /**
     *******************************************
     * LISTENERS
     * Listen for model change events that have been fired
     *******************************************
     */
    //puttting all this user and location stuff in rootScope as it's needed by other controller pretty frequently so actually wouldn't need all these watchers it seems
    //or we add watchers to all the interested controllers which would be cleaner actually
    //or we hold it all in a service as a Resource which is really how it should be done correctly
    $scope.$on('user:location:updated', function(event, location) {
        $rootScope.location = location;
        $log.debug(location);
    });

    $scope.$on('user:profile:updated', function(event, user) {
        $log.debug('User object retrieved: ',user);
        $rootScope.user = user;
        //let's see if we can authenticate them with the rest service.
        UserService.authenticateWithCloudinary().then(
            //success
            function(data) {
                $log.debug('user:authenticatedWithCloudinary: ' + data.data.name);
                var roles = data.data.roles;
                for(var i=0;i<roles.length;i++) {
                    if(roles[i] == 'editor') {
                        $rootScope.isEditor = true;
                    }
                }
            },
            //failure
            function(code) {
                //failed
                $log.warn('Failed to authenticate user with Cloudinary: ' + code.data);
            }
        );
    });

    $scope.$on('user:profile:logout', function() {
        $rootScope.user = null;
    });

    $scope.$on('user:profile:error', function() {
        $scope.flashMessage('popupBlocked');
    });

}); //end controller
    