'use strict';
/**
 * @description global controller to create portals scope inherited by all nested controllers.
 *
 * @class PortalsController
 * @param {Object} $log Angular logging framework
 * @param {Object} $scope Angular view model
 * @param {Object} $rootScope Angular scopse of all scopes
 * @param {Object} $timeout Angular wrapper for timeout
 * @param {Object} $location Angular wrapper for location
 * @param {Object} $window Angular wrapper for window
 * @param {Object} ContentService gets page and product and portal content
 * @param {Object} CONSTANTS application wide constants
 */
app.controller('rootCtrl', function($log, $scope, $timeout, CONSTANTS) {

    //toggles visibility of interface messages
    $scope.messages = {};

    /**
     * have the message enter and exit after three seconds
     *
     * @method flashMessage
     * @param {String} message to display
     */
    $scope.flashMessage = function(message) {
        $scope.messages[message] = true;
        $timeout(function() {
                $scope.messages[message] = false;
            },
            CONSTANTS.FLASH_TIME);
    };
}); //end controller