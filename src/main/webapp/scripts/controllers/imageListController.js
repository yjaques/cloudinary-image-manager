'use strict';

app.controller('imageListCtrl', function ($log, $scope, $rootScope, $location, ImageService, CONSTANTS, GeoLocateFactory, LocalJsonFactory) {

    // get the list of countries
    GeoLocateFactory.getCountries().then(
        //success
        function (countries) {
            $scope.countries = countries;
            //$log.debug(countries);
        },
        //failure so go ahead and try to get the google user
        function (code) {
        }
    );

    // get the list of themes
    LocalJsonFactory.getJson('data/themes.json').then(
        //success
        function (themes) {
            $scope.themes = themes;
            //$log.debug(themes);
        },
        //failure so go ahead and try to get the google user
        function (code) {
        }
    );

    //the possible copyright values
    $scope.copyright = [
        {"id": 'UNFPA',
            "label": "This photo was taken by a " + CONSTANTS.DOMAIN + " staff member and " + CONSTANTS.DOMAIN + " holds all use and transfer rights."
        },
        {"id": 'Commissioned',
            "label": "This photo was commissioned by " + CONSTANTS.DOMAIN + " but is non-transferable and credit must be shown."
        },
        {"id": 'Unknown',
            "label": "The photographer grants " + CONSTANTS.DOMAIN + " permission to use the image but check before using."
        }
    ];

    /**
     * user can delete within 10 minutes of upload
     * not currently using as we decided a regular user could delete any image as long as it was still in their moderation queue
     *
     * @method deleteByToken
     * @param image image to delete
     */
    /*$scope.deleteByToken = function (image) {
     $.cloudinary.delete_by_token(image.delete_token)
     .success(function (data) {
     $log.debug('Deleted image: ', data);
     //have to do this due to use of filter in view
     removeImageFromScope(image.public_id);
     //need to do this as the Jquery function is outside of Angular's digest
     $scope.$apply();
     $scope.flashMessage('imageDeleted');
     })
     .error(function (msg, code) {
     $log.error('Error deleting image: ' + msg, code);
     $scope.flashMessage('imageDeletedError');
     });
     };*/

    /**
     * delete an image
     *
     * @method deleteImage
     * @param id id of image to delete
     */
    $scope.deleteImage = function (id) {
        ImageService.deleteImage(id).then(
            //success
            function (data) {
                $log.debug('image:deleted');
                $rootScope.quota--;
                removeImageFromScope(id);
                $scope.flashMessage('imageDeleted');
            },
            //failure
            function (error) {
                $log.warn(error);
                $scope.flashMessage('imageDeletedError');
            }
        );
    };

    /**
     * update the metadata for an image
     *
     * @method updateImage
     * @param image image to update
     */
    $scope.updateImage = function (image) {
        $log.debug(image);
        ImageService.updateImage(image).then(
            //success
            function (data) {
                $log.debug('image:updated', data);
                $scope.flashMessage('imageUpdated');
            },
            //failure
            function (error) {
                $log.warn(error);
                $scope.flashMessage('imageUpdatedError');
            }
        );
    };

    /**
     * view all images awaiting moderation
     *
     * @method viewApprovalQueue
     * @param {String} cursor position from which to retrieve list of images
     */
    function viewApprovalQueue(cursor) {
        ImageService.getModerationQueue(cursor).then(
            //success
            function (data) {
                $log.debug('image:moderation queue', data);
                $scope.images = ImageService.cleanData(data.resources, 'pending');
                paginate(data);
            },
            //failure
            function (error) {
                $log.warn(error);
                $scope.flashMessage('fileFail');
            }
        );
    }

    /**
     * change the moderation status of an image
     * have to do this due to use of filter in view
     *
     * @method moderateImage
     * @param {Object} image the image to moderate
     * @param {String} status accepted or rejected
     * @param {String} channel whether to make it UNFPA or Public or remove from both
     */
    $scope.moderateImage = function (image, status, channel) {
        ImageService.moderateImage(image, status, channel).then(
            //success
            function (data) {
                $log.debug('image:moderated');
                removeImageFromScope(image.public_id);
                $scope.flashMessage('imageModerated', data);
            },
            //failure
            function (error) {
                $log.warn(error);
                $scope.flashMessage('imageModeratedError');
            }
        );
    };


    /**
     * toggle whether or not the image is PRINT quality as opposed to simply WEB quality
     * this is now calculated automatically by seeing if there are at least 2.1MPixels
     *
     * @method printQuality
     */
    /*$scope.printQuality = function (image) {
     $log.debug(image.context.custom.printQuality);
     image.context.custom.printQuality = (!(image.context.custom.printQuality === true || image.context.custom.printQuality === 'true'));
     $log.debug(image.context.custom.printQuality);
     $scope.updateImage(image);
     }*/

    /**
     * get all un-moderated (PENDDING) images associated with a particular user
     * in rootScope as it's called by the Navbar. There's surely a more elegant way to do this, probably using routing
     *
     * @method viewImagesByUser
     * @param cursor pagination value
     */
    function viewImagesByUser(cursor) {
        getImagesByTag($rootScope.user.data.primaryEmail, 'pending', cursor);
    }

    /**
     * get all images associated to a certain tag
     *
     * @method getImagesByTag
     * @param {String} tag tag name to search by
     * @param {String} status PENDING, APPROVED OR REJECTED
     * @param {String} cursor pagination position
     */
    function getImagesByTag(tag, status, cursor) {
        ImageService.getImagesByTagRest(tag, cursor).then(
            //success
            function (data) {
                $log.debug('images:images returned', data);
                $scope.images = ImageService.cleanData(data.resources, status);
                paginate(data);
            },
            //failure
            function (error) {
                $log.warn(error);
                $scope.flashMessage('fileFail');
            }
        );
    }

    /**
     * get all PUBLIC images
     *
     * @method getPublicImages
     * @param {String} cursor pagination position
     */
    function getPublicImages(cursor) {
        ImageService.getPublicImages(cursor).then(
            //success
            function (data) {
                $log.debug('images: public images returned', data);
                $scope.images = ImageService.cleanData(data.resources, 'approved');
                paginate(data);
            },
            //failure
            function (error) {
                $log.warn(error);
                $scope.flashMessage('fileFail');
            }
        );
    }

    /**
     * user cannot have more than ten images awaiting moderation, recursive function
     *
     * @method checkUserQuota
     * @param {int} count how many images in users mod queue
     */
    function checkUserQuota(count, cursor) {

        ImageService.getImagesByTagRest($rootScope.user.data.primaryEmail, cursor).then(
            //success
            function (data) {
                $log.debug('image:user images received, checking user count');
                //var count = 0;
                for (var i = 0; i < data.resources.length; i++) {
                    if (data.resources[i].moderation_status === 'pending') {
                        count++;
                        if (count === CONSTANTS.MAX_FILES) {
                            //we hit the limit!
                            $rootScope.quota = count;
                            $log.debug('user quota is: ', $rootScope.quota);
                            return;
                        }
                    }
                }
                if (data.next_cursor) {
                    //there are more images, we better keep checking
                    checkUserQuota(count, data.next_cursor);
                }
                $rootScope.quota = count;
                $log.debug('user quota is: ', $rootScope.quota);
                return;
            },
            //failure
            function (error) {
                $log.warn(error);
                $scope.flashMessage('checkQuotaFail');
            }
        );
    }

    /**
     * set pagination as required
     *
     * @method paginate
     * @param {Object} data the set of images
     */
    function paginate(data) {
        //check if we have a next_cursor value, if so find out where it is and set the pointer or if it doesn't exist push it on top of the stack
        if (data.next_cursor) {
            if ($scope.cursor) {
                for (var i = 0; i < $scope.cursor.length; i++) {
                    if (data.next_cursor === $scope.cursor[i]) {
                        $scope.cursorPos = i - 1;
                        $log.debug('$scope.cursor: ', $scope.cursor);
                        $log.debug('$scope.cursorPos: ', $scope.cursorPos);
                        return;
                    }
                }
                $scope.cursor.push(data.next_cursor);
                $scope.cursorPos = $scope.cursor.length - 2;
            } else {
                //initialize
                $scope.cursor = [data.next_cursor];
            }
        } else if ($scope.cursorPos != -1) {
            //we are on the last page
            $scope.cursorPos++;
            //$scope.cursor[$scope.cursorPos]='done';
        }
        $log.debug('$scope.cursor: ', $scope.cursor);
        $log.debug('$scope.cursorPos: ', $scope.cursorPos);
    }

    /**
     * iterate through collection and remove deleted image
     * have to do this due to use of filter in view
     *
     * @method removeImageFromScope
     * @param {string} id id of image to delete
     */
    function removeImageFromScope(id) {
        for (var i = $scope.images.length - 1; i >= 0; i--) {
            if ($scope.images[i].public_id == id) {
                $scope.images.splice(i, 1);
            }
        }
    }

    /**
     * copy image URL to clipboard
     *
     * @method copyUrlToClipboard
     * @param {String} url URL to copy to clipboard
     */
    $scope.copyUrlToClipboard = function (url) {
        var copyFrom = document.createElement("textarea");
        copyFrom.textContent = url;
        var body = document.getElementsByTagName('body')[0];
        body.appendChild(copyFrom);
        copyFrom.select();
        document.execCommand('copy');
        body.removeChild(copyFrom);
        $scope.flashMessage('linkCopied');
    };

    //loads the page
    function init() {
        //default cursorPos for pagination
        $scope.cursorPos = -1;
        $scope.cursor = [];

        if ($rootScope.view == 'org') {
            getImagesByTag('org', 'approved', null);
        } else if ($rootScope.view == 'public') {
            getPublicImages(null);
        } else if ($rootScope.isEditor === true && $location.path().indexOf('moderation') !== -1) {
            viewApprovalQueue(null);
        } else if ($rootScope.user) {
            viewImagesByUser(null);
        }
    }

    //INIT
    init();


    /**
     * LISTENERS
     *
     */
        //once we have a user we can go and get their images
    $scope.$on('user:profile:updated', function (event, user) {
        if (! $rootScope.isEditor) {
            checkUserQuota(0, null);
        }
        init();
    });

    //add image to list now that it is uploaded
    $scope.$on('image:uploaded', function (event, image) {
        //normalize values
        image.context.custom = ImageService.fixStringyValue(image.context.custom);
        image.context.custom.printQuality = ImageService.checkHiRes(image);
        $scope.images.push(image);

        //need to use apply since jquery is outside of angular's digest
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    });

    //remove images from scope if they match the tag value
    $scope.$on('image:deletedByTag', function (event, tag) {
        for (var i = 0; i < $scope.images.length; i++) {
            for (var j = 0; j < $scope.images[i].tags.length; j++) {
                if ($scope.images[i].tags[j] === tag) {
                    $scope.images.splice(i, 1);
                    i--;
                    break;
                }
            }
        }
    });
    //end controller
});