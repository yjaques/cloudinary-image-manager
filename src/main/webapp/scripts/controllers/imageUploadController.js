'use strict';

app.controller('imageUploadCtrl', function ($log, $scope, $location, $rootScope, $timeout, Upload, $filter, CONSTANTS, UtilService, ImageService) {

    /**
     * the Drag and drop upload
     *
     */

    $scope.$watch(function() {
        return $scope.files;
    }, function(newValue, oldValue) {
        if (newValue) {

            if (!$rootScope.isEditor && $rootScope.quota + newValue.length > CONSTANTS.MAX_FILES) {
                $scope.flashMessage('over10');
                fade('files');
                return;
            } else {
                $log.debug($rootScope.isEditor);
                $log.debug(newValue.length);
                //add to user quota
                $rootScope.quota = $rootScope.quota + newValue.length;

                // we need this set up correctly sa the return object has a bug in its JSON
                var context = {
                    lat: $rootScope.coords.latitude,
                    lng: $rootScope.coords.longitude,
                    location: $rootScope.location.name + ', ' + $rootScope.location.adminName1,
                    countryCode: $rootScope.location.countryCode,
                    credit: $rootScope.user.data.name.fullName,
                    copyright: 'UNFPA',
                    date: $filter('date')(new Date(), 'yyyy-MM-dd'),
                    printQuality: false
                };
                //formatted for Cloudinary upload
                //tags sets how the collection will be attached to a user
                //upload presets are in config.js
                var uploadData = {
                    upload_preset: $.cloudinary.config().upload_preset,
                    tags: $rootScope.user.data.primaryEmail + ',' + $rootScope.server,
                    context: 'lat=' + context.lat +
                        '|lng=' + context.lng +
                        '|location=' + context.location +
                        '|countryCode=' + context.countryCode +
                        '|credit=' + context.credit +
                        '|copyright=' + context.copyright +
                        '|date=' + context.date +
                        '|printQuality=' + context.printQuality
                };

                newValue.forEach(function (file) {

                    //the upload action
                    $scope.upload = Upload.upload({
                        url: "https://api.cloudinary.com/v1_1/" + $.cloudinary.config().cloud_name + "/upload",
                        fields: uploadData,
                        file: file
                    }).progress(function (e) {
                            file.progress = Math.round((e.loaded * 100.0) / e.total);
                            file.status = "Uploading... " + file.progress + "%";
                            if (!$scope.$$phase) {
                                //need to use apply since jquery is outside of angular's digest
                                $scope.$apply();
                            }
                        }).success(function (data, status, headers, config) {
                            //if the upload extracted IPTC/EXIF/XMP data we'll add some to our context fields
                            if (data.image_metadata) {
                                var imd = data.image_metadata;
                                var custom = context;

                                //title
                                if (imd.ObjectName || imd.Headline) {
                                    custom.title = imd.Headline || imd.ObjectName;
                                }

                                //keywords
                                if (imd.Keywords || imd.Category || imd.SupplementalCategories) {
                                    if (imd.Keywords) {
                                        custom.keywords = imd.Keywords;
                                    }
                                    if (imd.Category) {
                                        custom.keywords += ',' + imd.Category;
                                    }
                                    if (imd.SupplementalCategories) {
                                        custom.keywords += ',' + imd.SupplementalCategories;
                                    }
                                    custom.keywords = custom.keywords.replace(/^,(.+)$/, '$1');
                                }

                                //caption
                                if (imd.ImageDescription || imd['Caption-Abstract']) {
                                    custom.caption = imd.ImageDescription || imd['Caption-Abstract'];
                                }

                                //credit
                                if (imd['By-line']) {
                                    custom.credit = imd['By-line'];
                                    if (imd['By-lineTitle']) {
                                        custom.credit += ', ' + imd['By-lineTitle'];
                                    }
                                } else if (imd['Credit']) {
                                    custom.credit = imd['Credit'];
                                } else if (imd['Writer-Editor']) {
                                    custom.credit = imd['Writer-Editor'];
                                }

                                //copyright - override UNFPA if there is a source value
                                if (imd.CopyrightNotice || imd.Source) {
                                    custom.copyright = imd.CopyrightNotice || imd.Source;
                                }

                                //date - use image date if available
                                if (imd.DateCreated) {
                                    custom.date = imd.DateCreated.replace(':', '-');
                                }

                                //Location/lat/lng - use image GPS if available
                                if (imd.GPSLongitude) {
                                    //normalize
                                    var lng = imd.GPSLongitude.replace(/(deg )|'|"|\\/g, '').split(' ');
                                    var lat = imd.GPSLatitude.replace(/(deg )|'|"|\\/g, '').split(' ');
                                    custom.lat = UtilService.convertDMSToDD(lat[0], lat[1], lat[2], lat[3]);
                                    custom.lng = UtilService.convertDMSToDD(lng[0], lng[1], lng[2], lng[3]);
                                    //otherwise use location metadata if available
                                } else if (imd['Country-PrimaryLocationName']) {
                                    custom.location = '';
                                    if (imd['Sub-location']) {
                                        custom.location = imd['Sub-location'] + ', ';
                                    }
                                    if (imd.City) {
                                        custom.location += imd.City + ', ';
                                    }
                                    if (imd['Province-State']) {
                                        custom.location += imd['Province-State'];
                                    }
                                }

                                if (imd['Country-PrimaryLocationCode']) {
                                    custom.countryCode = imd['Country-PrimaryLocationCode'];
                                }
                            }

                            data.context.custom = custom;

                            $log.debug('image:uploaded', data);
                            //now let's rewrite the metadata fields to our liking
                            ImageService.updateImage(data).then(
                                //success
                                function (data) {
                                    $rootScope.$broadcast('image:uploaded', data);
                                    $scope.flashMessage('imagesUploaded');
                                    $log.debug('image:updated', data);
                                },
                                //failure
                                function (error) {
                                    $log.error('method: uploadimage, msg: ', error);
                                    $scope.flashMessage('imagesUpLoadedError');
                                }
                            );

                            //fade the upload porgress/result screen
                            fade('files');
                        }).error(function (msg, code) {
                            // called asynchronously if an error occurs
                            // or server returns response with an error status.
                            $log.error('method: uploadimage, msg: ', msg, code);
                            $scope.flashMessage('imagesUploadedError');
                        });
                });
            }
        }
    });

    /**
     * timed fade
     *
     * @method fade
     * @param {string} what name of $scope value to destroy after timeout
     */
    function fade(what) {
        $timeout(function () {
                $scope[what] = null;
            },
            CONSTANTS.FLASH_TIME);
    }

    /**
     * Modify the look of the drop zone when files are being dragged over it
     */
    $scope.dragOverClass = function ($event) {
        var items = $event.dataTransfer.items;
        var hasFile = false;
        if (items != null && $rootScope.quota + items.length <= CONSTANTS.MAX_FILES) {
            for (var i = 0; i < items.length; i++) {
                if (items[i].kind == 'file') {
                    hasFile = true;
                    break;
                }
            }
        } else {
            hasFile = true;
        }
        return hasFile ? "dragover" : "dragover-err";
    };

//end controller
})
;