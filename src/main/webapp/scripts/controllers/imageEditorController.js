'use strict';

app.controller('imageEditorCtrl', function ($log, $scope, $rootScope, ImageService, CONSTANTS) {

    /**
     * delete all images that have a certain tag associated to them
     *
     * @method deleteImagesByTag
     */
    $scope.deleteImagesByTag = function () {
        $log.debug('selected tag: ', $scope.selectedTag);
        if (CONSTANTS.PERMA_TAGS.indexOf($scope.selectedTag) === -1) {
            if (confirm('Really delete all images associated with this tag?')) {
                ImageService.deleteImagesByTag($scope.selectedTag).then(
                    //success
                    function (data) {
                        $log.debug('Images deleted by tag', data);
                       //remove images from scope
                        $rootScope.$broadcast('image:deletedByTag', $scope.selectedTag);
                        $scope.flashMessage('imagesDeleted');
                        //remove tag from scope
                        $scope.tags.splice($scope.tags.indexOf($scope.selectedTag), 1);
                        $scope.selectedTag = null;
                    },
                    //failure
                    function (error) {
                        $log.warn(error);
                        $scope.flashMessage('Failed to delete images by tag');
                    }
                );
            } else {
                $scope.selectedTag = null;
            }
        } else {
            $scope.flashMessage('permaTag');
        }
    };

    //get the list of tags so that images can be deleted by tag
    function init() {
        ImageService.listTags().then(
            //success
            function (data) {
                $log.debug('tags returned', data.tags);
                $scope.tags = data.tags;
                //remove any tags that we don't permit deletion of like UNFPA and public channel
                for(var i=0; i<$scope.tags.length; i++) {
                    if(CONSTANTS.PERMA_TAGS.indexOf($scope.tags[i]) !== -1) {
                        $scope.tags.splice(i,1);
                        i--;
                    }
                }
                $log.debug('tags after splicing', $scope.tags);
            },
            //failure
            function (error) {
                $log.warn(error);
            }
        );
    }

    init();

//end controller
})
;