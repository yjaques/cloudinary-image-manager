'use strict';
/**
 * @description returns product profiles
 *
 * @class UserService
 * @param {Object} $q Angular deferred library
 * @param {Object} $log Angular console log wrapper
 * @param {Object} $rootScope Global Angular view model
 * @param {Object} $cookies Angular cookie wrapper
 * @param {Object} $location Angular location wrapper
 * @param {Object} $window Angular window wrapper
 * @param {Object} geolocation asynchronous module to geolocate using HTML5
 * @param {Object} GeoLocateFactory Given lat/long it returns the users address location
 * @param {Object} UserFactory Gets the user profile object
 * @param {Object} $auth OAUTH2 service
 * @param (Object) CONSTANTS app constants
 */
app.service('UserService', function ($q, $log, $rootScope, $cookies, $location, $window, $http, geolocation, GeoLocateFactory, UserFactory, $auth, CONSTANTS) {
    return {
        /**
         * Use HTML5 geo-location lat/long to reverse geolocate
         *
         * @method getUser
         */
        getUser: function() {
            //return the user object if we already have one
            if($rootScope.hasOwnProperty('user')) {
                return $rootScope.user
            }
            var self = this;
                self.getGoogleLogin();
        },

        /**
         * authenticates the user with Google
         *
         * @method getGoogleLogin
         */
        getGoogleLogin: function() {
            var self = this;
            if(! $auth.isAuthenticated()) {
                //user needs to log
                var url = $location.url();
                $log.debug(url);
                $auth.authenticate('google').then(
                    function(response) {
                        // Signed In
                        //have to reset this as satellizer kills it
                        $location.url(url);
                        $log.debug(response);
                        self.getGoogleUser();
                    },
                    function(error) {
                        $location.url(url);
                        $log.error(error);
                        //warn user they probably have their OAuth popup blocked
                        $rootScope.$broadcast('user:profile:error');
                        self.getMockUser();
                    }
                );
            } else {
                self.getGoogleUser();
            }
        },

        /**
         * gets Google user profile
         * note that since Satellizer's httpInterceptor adds an Auth header on with the value from local storage there is no need to pass along the auth info in this function
         *
         * @method getGoogleUser
         */
        getGoogleUser: function() {
            var self = this;
            UserFactory.getGoogleUser(CONSTANTS.GOOGLE_PROFILE_URL).then(
                //success
                function(user) {
                    //process and inform the controller if in the meanwhile we haven't gotten an internal product user loaded
                    if (! $rootScope.appAuthUser) {
                        //rewrite json to match the TED IDM json
                        user.data = {name: {fullName: user.name}, thumbnailimageUrl: user.picture, primaryEmail: user.email};
                        $log.debug('user:profile:updated', user);
                        $rootScope.$broadcast('user:profile:updated', user);
                    }
                },
                //failure
                function(code) {
                    if(code===401) {
                        //unauthorized so clean the token and send user to the google login
                        $auth.removeToken();
                        self.getGoogleLogin();
                    } else {
                        self.getMockUser();
                    }
                }
            );
        },

        /**
         * get the user details from the embedded app
         *
         * @method getAppAuthUser
         */
        getAppAuthUser: function(userPath) {
            UserFactory.getAppAuthUser(userPath).then(
                //success
                function(user) {
                    $rootScope.appAuthUser = true;
                    $log.debug('user:profile:updated: ');
                    $rootScope.$broadcast('user:profile:updated', user);
                },
                //failure
                function(code) {
                    $log.warn('Failed to get AppAuth user, rolling back to previous profile. Code: ' + code);
                    //do nothing as we should have already have either the IDM or Google login
                }
            );
        },

        /**
         * gets a mock user
         *
         * @method getMockUser
         */
        getMockUser: function() {
            UserFactory.getMockUser(CONSTANTS.MOCK_USER_PATH).then(
                //success
                function(user) {
                    $log.debug('user:profile:updated');
                    $rootScope.$broadcast('user:profile:updated', user);
                },
                //failure
                function(code) {
                    //failed
                    $log.warn('Failed to get Mock user. Code: ' + code);
                }
            );
        },


        /**
         * get the user details from the embedded app
         *
         * @method getAppAuthUser
         */
        authenticateWithCloudinary: function() {
            var deferred = $q.defer();

            var token = localStorage.getItem('satellizer_token');

            var url = $rootScope.restUrl + 'authenticate/' + token;

            $http.get(url, null).
                success(function (data) {
                    // this callback will be called asynchronously
                    // when the response is available
                    deferred.resolve(data);
                }).
                error(function (msg, code) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                    $log.error('method: authenticate, msg: ', msg);
                    deferred.reject(code);
                });
            return deferred.promise;
        },

        /**
         * logout user
         *
         * @method logout
         */
        logout: function() {
            if($auth.isAuthenticated()) {
                $auth.logout();
                $log.debug('user:profile:logout');
                $rootScope.$broadcast('user:profile:logout');
            }
        },

        /**
         * Gets lat/long from the HTML5 geolocation service
         *
         * @method getLocation
         * @return promise to return product
         */
        getLocation: function () {
            var self = this;
            //asynchronous call to geolocation
            geolocation.getLocation().then(
                //success
                function(data){
                    $log.debug('lat/lng coords are: ', data);
                    $rootScope.coords = data.coords;
                    self.getAddress(data);
                    return true;
                },
                //failure
                function(error){
                    $log.warn(error);
                    //set Greenwich and call the normal callback
                    $rootScope.coords = {'latitude': 0,'longitude': 0};
                    self.getAddress({coords: $rootScope.coords});
                    return false;
                }
            );
        },

        /**
         * Use HTML5 geo-location lat/long to reverse geoLocate
         *
         * @method getAddress
         * @param {Object} position user lat/long
         */
        getAddress: function(position) {
            //geolocate returns promise of an address
            GeoLocateFactory.getAddress(position.coords.latitude, position.coords.longitude).then(
                //success
                function(geo) {
                    $log.debug('user:location:updated');
                    $rootScope.$broadcast('user:location:updated', geo.geonames[0]);
                },
                //failure
                function(error) {
                    $log.warn(error);
                    //self.getGoogleAddress(position);
                }
            );

        },

        /**
         * Use HTML5 geo-location lat/long to reverse geoLocate
         *
         * @method getGoogleAddress
         * @param {Object} position user lat/long
         */
        getGoogleAddress: function(position) {
            //geolocate returns promise of an address
            GeoLocateFactory.getGoogleAddress(position.coords.latitude, position.coords.longitude).then(
                //success
                function(geo) {
                    $log.debug('user:location:updated', geo.results[0]);
                    //reformat the google format to fit the GeoNames format
                    if (geo.results.length > 0) {
                        var geonames = geo.results[0].address_components;
                        var geoObj = {
                            'name': geonames[2].short_name,
                            'adminName1':geonames[geonames.length-3].long_name,
                            'countryName': geonames[geonames.length-2].short_name
                        };
                    } else {
                        var geoObj = {
                            'name': 'NY',
                            'adminName1': 'New York',
                            'countryName': 'US'
                        };
                    }
                    $rootScope.$broadcast('user:location:updated', geoObj);
                },
                //failure
                function(error) {
                    $log.warn(error);
                    $rootScope.$broadcast('user:location:error', {'name': 'New York','countryName': 'USA'});
                }
            );
        }
    };
});