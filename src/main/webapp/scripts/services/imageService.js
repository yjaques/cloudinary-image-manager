'use strict';

//goes and retrieves imagess
app.factory('ImageService', function ($log, $rootScope, $resource, $http, $q) {
    return {
        /**
         * Gets all images that match a particular tag (up to 100)
         * uses the anonymous method and needs no back end service
         * not currently used as you can't tweak the return values that are included as you can using the JAVA ADMIN API
         *
         * @method getImagesByTag
         */
       /* getImagesByTag: function (tag) {

            var url = $.cloudinary.url(tag, {format: 'json', type: 'list'});

            //cache buster on Cloudinary side
            url = url + "?" + Math.ceil(new Date().getTime() / 1000);
            $log.debug(url);
            return $resource(url);
        }, */

        /**
         * Gets all images that match a particular tag (up to 100)
         * Uses back end service
         *
         * @method getImagesByTag
         * @param {String} tag tag name for which to get the images
         * @param {String} cursor nextCursor point form which to fetch images if any
         */
        getImagesByTagRest: function (tag, cursor) {
            var self = this;
            var deferred = $q.defer();
            var url = $rootScope.restUrl + 'list_images_by_tag/' + tag;
            var config = {};
            if (cursor !== null) {
                config.params = {nextCursor: cursor};
            }
            $http.get(url, config).
                success(function (data) {
                    // this callback will be called asynchronously
                    // when the response is available
                    deferred.resolve(self.fixStringyValues(data));
                }).
                error(function (msg, code) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                    $log.error('method: getImagesByTagRest, msg: ', msg);
                    deferred.reject(code);
                });
            return deferred.promise;
        },


        /**
         * Gets all images that match a particular tag (up to 100)
         * Uses back end service
         *
         * @method getImagesByTag
         * @param {String} tag tag name for which to get the images
         * @param {String} cursor nextCursor point form which to fetch images if any
         */
        getPublicImages: function (cursor) {
            var self = this;
            var deferred = $q.defer();
            var url = $rootScope.restUrl + 'list_public_images';
            var config = {};
            if (cursor !== null) {
                config.params = {nextCursor: cursor};
            }
            $http.get(url, config).
                success(function (data) {
                    // this callback will be called asynchronously
                    // when the response is available
                    deferred.resolve(self.fixStringyValues(data));
                }).
                error(function (msg, code) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                    $log.error('method: getPublicImages, msg: ', msg);
                    deferred.reject(code);
                });
            return deferred.promise;
        },

        /**
         * Gets all images that match a particular tag (up to 50)
         * Uses back end service
         *
         * @method getModerationQueue
         * @param {String} cursor nextCursor point form which to fetch images if any
         */
        getModerationQueue: function (cursor) {
            var self = this;
            var deferred = $q.defer();
            var url = $rootScope.restUrl + 'list_images_to_be_moderated';
            var config = {};
            if (cursor !== null) {
                config.params = {nextCursor: cursor};
            }
            $http.get(url, config).
                success(function (data) {
                    // this callback will be called asynchronously
                    // when the response is available
                    deferred.resolve(self.fixStringyValues(data));
                }).
                error(function (msg, code) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                    $log.error('method: getModerationQueue, msg: ', msg);
                    deferred.reject(code);
                });
            return deferred.promise;
        },

        /**
         * Update image metadata
         *
         * @method updateImage
         * @param {Object} image the image to update
         */
        updateImage: function (image) {
            var deferred = $q.defer();

            var url = $rootScope.restUrl + 'update_image/' + image.public_id.replace(/\//g, '%2F');
            var form = {
                tags: image.tags,
                context: image.context
            };
            $log.debug(form);
            // Simple POST request example (passing data) :
            $http.post(url, form).
                success(function (data, status, headers, config) {
                    // this callback will be called asynchronously
                    // when the response is available
                    deferred.resolve(data);
                }).
                error(function (data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                    $log.error('method: updateImage, msg: ', data);
                    deferred.reject(data);
                });
            return deferred.promise;
        },

        /**
         * Moderate image
         *
         * @method moderateImage
         * @param {Object} image the image to moderate
         * @param {String} status accepted or rejected
         * @param {String} channel whether to make it UNFPA or Public
         */
        moderateImage: function (image, status, channel) {
            var deferred = $q.defer();
            var url = $rootScope.restUrl + 'moderate_image/' + image.public_id.replace(/\//g, '%2F');
            var config = {params: {status: status}};
            if (channel !== null) {
                image.tags.push(channel);
                config.params.tags = image.tags.toString();
            }

            $http.get(url, config).
                success(function (data) {
                    // this callback will be called asynchronously
                    // when the response is available
                    deferred.resolve(data);
                }).
                error(function (msg, code) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                    $log.error('method: moderateImage, msg: ', msg);
                    deferred.reject(code);
                });
            return deferred.promise;
        },

        /**
         * delete image
         *
         * @method deleteImage
         * @param {String} id the id of theimage to delete
         */
        deleteImage: function (id) {
            var deferred = $q.defer();

            //var token = localStorage.getItem('satellizer_token');

            var url = $rootScope.restUrl + 'delete_image/' + id.replace(/\//g, '%2F');

            $http.get(url, null).
                success(function (data) {
                    // this callback will be called asynchronously
                    // when the response is available
                    deferred.resolve(data);
                }).
                error(function (msg, code) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                    $log.error('method: deleteImage, msg: ', msg);
                    deferred.reject(code);
                });
            return deferred.promise;
        },

        /**
         * delete images by tag
         *
         * @method deleteImagesByTag
         * @param {String} tag the tag name of the images to delete
         */
        deleteImagesByTag: function (tag) {
            var deferred = $q.defer();

            //var token = localStorage.getItem('satellizer_token');

            var url = $rootScope.restUrl + 'delete_images_by_tag/' + tag;

            $http.get(url, null).
                success(function (data) {
                    // this callback will be called asynchronously
                    // when the response is available

                    deferred.resolve(data);
                }).
                error(function (msg, code) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                    $log.error('method: deleteImagesByTag, msg: ', msg);
                    deferred.reject(code);
                });
            return deferred.promise;
        },

        /**
         * list all tags
         *
         * @method listTags
         */
        listTags: function () {
            var deferred = $q.defer();
            var url = $rootScope.restUrl + 'list_tags';

            $http.get(url, null).
                success(function (data) {
                    // this callback will be called asynchronously
                    // when the response is available
                    deferred.resolve(data);
                }).
                error(function (msg, code) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                    $log.error('method: listTags, msg: ', msg);
                    deferred.reject(code);
                });
            return deferred.promise;
        },

        /**
         * fix a set of context.custom objects to make their values boolean and remove nulls as needed
         *
         * @method fixStringyValue
         * @param (Object) data the set of images to fix
         * @return (Object) fixed data set
         */
        fixStringyValues: function (data) {
            var self = this;
            //clean out stringy null values
            for (var i = 0; i < data.resources.length; i++) {
                data.resources[i].context.custom = self.fixStringyValue(data.resources[i].context.custom);
                data.resources[i].context.custom.printQuality = self.checkHiRes(data.resources[i]);
            }
            return data;
        },

        /**
         * multiply widthxhieght and if it is 2.1MPixels it is hi-res
         *
         * @method setHiRes
         * @param (Object) image image to check
         * @return (Boolean) true if hi-res
         */
        checkHiRes: function (image) {
            //if HD quality tag it hi-res
            if(image.height*image.width >= 2073600) {
                return true;
            }
        },

        /**
         * fix a single context.custom object to make its values boolean and remove nulls as needed
         *
         * @method fixStringyValue
         * @param (Object) metadata metadata to fix
         * @return (Object) fixed metadata
         */
        fixStringyValue: function (metadata) {
            for (var key in metadata) {
                if (metadata.hasOwnProperty(key)) {
                    //remove null values
                    if (metadata[key] === 'null') {
                        delete(metadata[key]);
                    }
                    //turn stringy boolean into real boolean
                    if (('truefalse').indexOf(metadata[key]) !== -1) {
                        metadata[key] = !!(metadata[key] === 'true');
                    }
                }
            }
            //convert stringy date to a real date object
            metadata.date = new Date(metadata.date);
            //lower-case the keywords
            if(metadata.hasOwnProperty('keywords')) {
                metadata.keywords = metadata.keywords.toLowerCase();
            }
            return metadata;
        },


        /**
         * remove images that have been rejected (unless asked for specifically in status)
         * or images that do not match the requested status (approved or pending)
         *
         * @method cleanData
         * @param {Object} images set of images to clean
         * @param {String} status approved, rejected or pending
         * @return (Object) cleaned set of images
         */
        cleanData: function (images, status) {
            //loop backwards cause splicing
            for (var i = images.length - 1; i > -1; i--) {
                if (images[i].moderation_status === 'rejected' && status !== 'rejected' ||
                    status !== null && images[i].moderation_status !== status) {
                    images.splice(i, 1);
                }
            }
            return images;
        }
    }
});