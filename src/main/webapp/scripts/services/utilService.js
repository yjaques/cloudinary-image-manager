'use strict';
/**
 * @description Utility functions
 *
 * @class UtilService
 * @param {Object} the class
 */
app.service('UtilService', function($rootScope) {
    return {
        /**
         * Converts a NodeList into an array.
         *
         * @method toArray
         * @param {NodeList} list The array-like object.
         * @return {Array} The NodeList as an array.
         */
        toArray: function(list) {
            return Array.prototype.slice.call(list || [], 0);
        },

        /**
         * Converts an EXIF date to an ISO date
         *
         * @method exifToIso
         * @param {Date} date The date to convert
         * @return {Date} The converted date
         */
        exifToIso: function(date) {
            return date.replace(/^(.+):(.+):(.+) .+$/, '$1-$2-$3');
        },

        /**
         * return a date in the past, each period is a day
         *
         * @method getPeriod
         * @param {Integer} period number of days in the past
         * @return {Number}  Date that is period number of days in the past
         */
        getPeriod: function(period) {
            return new Date().getTime() - 86400000 * period;
        },

        /**
         * sort the file entries by date
         *
         * @method sortByDate
         * @param {Object} a entry to sort
         * @param {Object} b entry to sort
         * @return {Integer} whether a is younger than b
         */
        sortByDate: function(a, b) {
            if (a.modifiedDate < b.modifiedDate) {
                return 1;
            }
            if (a.modifiedDate > b.modifiedDate) {
                return -1;
            }
            return 0;
        },

        /**
         * sort the file entries by title
         *
         * @method sortByTitle
         * @param {Object} a entry to sort
         * @param {Object} b entry to sort
         * @return {Integer} whether a is alphabetically before b
         */
        sortByTitle: function(a, b) {
            if (a.title < b.title) {
                return 1;
            }
            if (a.title > b.title) {
                return -1;
            }
            return 0;
        },
        /**
         * sort the object by key name
         *
         * @method sortByKey
         * @param {Object} a entry to sort
         * @param {Object} b entry to sort
         * @return {Integer} whether a is before b
         */
        sortByKey: function(a, b) {
            if (a.key > b.key) {
                return 1;
            }
            if (a.key < b.key) {
                return -1;
            }
            return 0;
        },
        /**
         * make an array have unique values
         *
         * @method arrayToSet
         * @param {Object} a entry to sort
         * @return {Array} unique values
         */
        bagToSet: function(a) {
            return a.filter(function(item, pos, self) {
                return self.indexOf(item) === pos;
            });
        },

        /**
         * make an array have unique values
         *
         * @method arrayToSet
         * @param {Object} a entry to sort
         * @return {Array} unique values
         */
        convertDMSToDD:function(degrees, minutes, seconds, direction) {
            var dd = degrees + minutes/60 + seconds/(60*60);
            if (direction == "S" || direction == "W") {
                dd = dd * -1;
            } // Don't do anything for N or E
            return dd;
        },

        /**
         * set params needed by shared views from the routeProvider,
         * this seems the cleanest way to do something which really looks like a missing functionality in Angular!
         *
         * @method setRootParams
         * @param {Array} k/v array of params to set
         */
        setRootParams:function(params) {
            for(var key in params) {
                $rootScope[key] = params[key];
            }
        }
    };
});