'use strict';
/**
 * @description gets local JSON files so you can keep your data separate from your code
 *
 * @class LocalJsonFactory
 * @param {Object} $log Angular logging framework
 * @param {Object} $http Angular request response object
 * @param {Object} $q Angular deferred for asynchronous patterns
 */
app.factory('LocalJsonFactory', function($http, $log, $q) {
    return {
    /**
     * gets local JSON
     *
     * @method getJson
     * @return promise to return Json
     */
    getJson: function(filePath) {
        var deferred = $q.defer();
        $http.get(filePath)
            .success(function(data) {
                deferred.resolve(data);
            })
            .error(function(msg, code) {
                $log.error('Error getting files: ' + msg, code);
                deferred.reject(msg);
            });
        return deferred.promise;
    }
}
});