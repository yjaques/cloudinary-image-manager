'use strict';
/**
 * @description get the user's address from the lat long info
 *
 * @class GeoLocateFactory
 * @param {Object} $log Angular logging framework
 * @param {Object} $http Angular request response object
 * @param {Object} $q Angular deferred for asynchronous patterns
 */
app.factory('GeoLocateFactory', function($http, $log, $q, $location, CONSTANTS) {
  return {
    /**
     * get the user's address from the lat long info
     *
     * @method getAddress
     * @param {Number} latitude user's latitude
     * @param {Number} longitude user's longitude
     * @return promise to return address
     */
    getAddress: function(latitude, longitude) {
      var deferred = $q.defer();
      var config = {
        params: {
          'lat': latitude,
          'lng': longitude,
          'username': 'unfpa'
        }
      };
      $http.get($location.protocol() + '://api.geonames.org/findNearbyPlaceNameJSON', config)
        .success(function(data) {
          $log.debug('Location is: ', data);
          deferred.resolve(data);
        })
        .error(function(msg, code) {
          $log.error('GeoNames Geolocate error: ' + msg, code);
          deferred.reject(msg);
        });
      return deferred.promise;
    },

    /**
     * get the user's address from the lat long info
     *
     * @method getGoogleAddress
     * @param {Number} latitude user's latitude
     * @param {Number} longitude user's longitude
     * @return promise to return address
     */
    getGoogleAddress: function(latitude, longitude) {
        var deferred = $q.defer();
        var config = {
            params: {
                'latlng': latitude + ',' + longitude,
                'key': CONSTANTS.GOOGLE_API_KEY
            }
        };
        $http.get('https://maps.googleapis.com/maps/api/geocode/json', config)
            .success(function(data) {
                $log.debug('Location is: ', data);
                deferred.resolve(data);
            })
            .error(function(msg, code) {
                $log.error('Geolocate error: ' + msg, code);
                deferred.reject(msg);
            });
        return deferred.promise;
    },
      /**
       * get the list of countries
       *
       * @method getCountries
       * @return promise to return address
       */
      getCountries: function() {
          var deferred = $q.defer();
          $http.get('data/countries.json')
              .success(function(data) {
                  deferred.resolve(data);
              })
              .error(function(msg, code) {
                  $log.error('Error getting countries error: ' + msg, code);
                  deferred.reject(msg);
              });
          return deferred.promise;
      }
  };
});

