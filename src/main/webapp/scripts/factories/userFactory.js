'use strict';
/**
 * @description returns user profiles
 *
 * @class UserFactory
 * @param {Object} $http Angular request response object
 * @param {Object} $log Angular logging framework
 * @param {Object} $q Angular deferred for asynchronous patterns
 */
app.factory('UserFactory', function ($http, $log, $q) {
    return {

        /**
         * Gets Google+ user profile using OAUTH2
         *
         * @method getGogleUser
         * @param {String} googleUrl the URL to the google profile web service
         * @return promise to return user
         */
        getGoogleUser: function (googleUrl) {
            var deferred = $q.defer();
            var config = {
                params: {
                    'alt': 'json'
                },
                auth: true
            };
            $http.get(googleUrl, config)
                .success(function (data) {
                    deferred.resolve(data);
                })
                .error(function (msg, code) {
                    $log.error('method: getGoogleUser, msg: ', msg, code);
                    deferred.reject(code);
                });
            return deferred.promise;
        },


        /**
         * Return a mock user when the service fails (just used for testing, should be removed later)
         *
         * @method getMockUser
         * @return Mock user
         */
        getMockUser: function (mockUserPath) {
            var deferred = $q.defer();
            $http.get(mockUserPath)
                .success(function (data) {
                    deferred.resolve(data);
                })
                .error(function (msg, code) {
                    $log.error('method: getMockUser, msg: ', msg, code);
                    deferred.reject(code);
                });
            return deferred.promise;
        }
    };
});

