'use strict';

/* App Module */
var app = angular.module('app', ['ngRoute', 'ngResource', 'ngCookies', 'ngAnimate', 'ngMessages', 'geolocation', 'satellizer', 'cloudinary', 'ngFileUpload', 'confirmClick']);

/**
 * @description constants
 * @object CONSTANTS
 * @property {string} CLIENT_ID google ID to access Google APIs, should have one for every app that connects to Google
 * @property {string} GOOGLE_PROFILE_URL url to the Google profile web service
 * @property {string} GOOGLE_API_KEY key to use Google geoLocation web service
 * @property {string} DOMAIN domain of the organization
 * @property {boolean} Are we in dev or production?
 * @property {boolean} GEO_LOCATE Whether we must geoLocate the user
 * @property {int} FLASH_TIME Time in milliseconds to show messages
 * @property {int} MAX_FILES How many files a user can upload in a day
 * @property {string} PERMA_TAGS Tags that cannot be removed from a photo
 */
app.constant('CONSTANTS', {
    ClIENT_ID: '1087983827257-eii0c9sqhqoqhclp1uim62r5hfikchsg.apps.googleusercontent.com',
    GOOGLE_PROFILE_URL: 'https://www.googleapis.com/userinfo/v2/me',
    GOOGLE_API_KEY: '',
    DOMAIN: 'gmail.com',
    PROD: false,
    GEO_LOCATE: true,
    FLASH_TIME: 5000,
    MAX_FILES: 10,
    PERMA_TAGS: 'org,public'
});

app.config(function ($authProvider, CONSTANTS, $locationProvider, $logProvider, $routeProvider) {
    //enable debug level console log outputs
    $logProvider.debugEnabled(true);

    //needed so URls are not hashbanged. Could cause problems in older browsers?
   /*  $locationProvider.html5Mode({
     enabled: true,
     requireBase: true
     });  */

    //google auth configuration for Satellizer
    //note that we need to use the BASE element value to get the context for the redirect URI
    $authProvider.google({
        clientId: CONSTANTS.ClIENT_ID,
        redirectUri: window.location.origin || window.location.protocol + '//' + window.location.host,
        responseType: 'token',
        scopePrefix: 'openid'
    });

    $routeProvider
        .when('/gallery_public', {
            templateUrl: 'views/image-gallery.html',
            resolve: {
                view: function(UtilService){
                    UtilService.setRootParams({view:'public'});
                }
            }
        })
        .when('/gallery_org', {
            templateUrl: 'views/image-gallery.html',
            resolve: {
                user: function(UserService){
                    UserService.getUser();
                },
                view: function(UtilService){
                    UtilService.setRootParams({view:'org'});
                }
            }
        })
        .when('/image_upload', {
            templateUrl: 'views/image-upload.html',
            resolve: {
                user: function(UserService){
                    UserService.getUser();
                },
                view: function(UtilService){
                    UtilService.setRootParams({view:'upload',galleryTitle:'Uploaded Images'});
                }
            }
        })
        .when('/image_moderation', {
            templateUrl: 'views/image-editor.html',
            resolve: {
                user: function(UserService){
                    UserService.getUser();
                },
                view: function(UtilService){
                    UtilService.setRootParams({view:'moderate',galleryTitle:'Moderate Images'});
                }
            }
        })
        .otherwise({
            redirectTo: '/gallery_public'
        });
});

app.run(function ($rootScope, $location, $window, CONSTANTS) {

    //satellizer seems to sometimes mess up the redirect URI so we have to force it closed
    if ($location.url().indexOf('access_token=') !== -1) {
        //messed up redirect!
        window.close();
    }

    $rootScope.server='appDev';

    //are we in production?
    //just used to generate a different tag against the images so you can easily remove images uploaded during testing.
    if (CONSTANTS.PROD) {
        $rootScope.server = 'appProd';
    }

    $rootScope.domain = CONSTANTS.DOMAIN;

    //editors have a different role and functions.
    $rootScope.isEditor = false;

    //current uploading quota
    $rootScope.quota = 0;

    //max files
    $rootScope.maxFiles = CONSTANTS.MAX_FILES;

    //whether or not to show only print quality images
    $rootScope.filter = {
        printQuality: false,
        tag:null
    };

    //fix the base restUrl we will be sending to RestX
    $rootScope.restUrl = $location.absUrl().replace(/^(.+)#.+$/,'$1')  + 'api/';

});

/**
 * this filter will modify the tag display, just used for emails right now
 */
app.filter('cleanTag', function() {
    return function(input) {
        if (input) {
            return input.replace(/(.+)@.+/,'@$1');
        }
    };
});
