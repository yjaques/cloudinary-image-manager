package com.organicdesktop.cloudinary;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Charsets;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.organicdesktop.cloudinary.domain.RestUser;
import restx.*;
import restx.config.ConfigLoader;
import restx.config.ConfigSupplier;
import restx.factory.Module;
import restx.factory.Provides;
import restx.http.HttpStatus;
import restx.security.*;

import javax.inject.Named;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Paths;

@Module
public class AppModule {
    Logger logger = LoggerFactory.getLogger("AppModule");

    public static Optional<RestxPrincipal> currentUser() {
        return (Optional<RestxPrincipal>) RestxSession.current().getPrincipal();

    }

    @Provides
    public SignatureKey signatureKey() {
         return new SignatureKey("RestCloudinary 32170ccc-0b95-4da9-b730-22c99c8b5ceb cloudinary-rest -8956902943222201908".getBytes(Charsets.UTF_8));
    }

    @Provides
    @Named("restx.admin.password")
    public String restxAdminPassword() {
        //password to access the RestX web interface
        return "7448";
    }

    @Provides
    public ConfigSupplier appConfigSupplier(ConfigLoader configLoader) {
        // Load settings.properties in com.organicdesktop.cloudinary package as a set of config entries
        return configLoader.fromResource("com/organicdesktop/cloudinary/settings");
    }

   @Provides
    public CredentialsStrategy credentialsStrategy() {
        return new BCryptCredentialsStrategy();
    }

    @Provides
    public BasicPrincipalAuthenticator basicPrincipalAuthenticator(
            SecuritySettings securitySettings, CredentialsStrategy credentialsStrategy,
            @Named("restx.admin.passwordHash") String defaultAdminPasswordHash, ObjectMapper mapper) throws URISyntaxException {
        return new StdBasicPrincipalAuthenticator(new StdUserService<>(
                // use file based users repository.
                // Developer's note: prefer another storage mechanism for your users if you need real user management
                // and better perf
                new FileBasedUserRepository<>(
                        RestUser.class, // this is the class for the User objects, that you can get in your app code
                        // with RestxSession.current().getPrincipal().get()
                        // it can be a custom user class, it just need to be json deserializable
                        mapper,

                        // this is the default restx admin, useful to access the restx admin console.
                        // if one user with restx-admin role is defined in the repository, this default user won't be
                        // available anymore
                        new RestUser("admin", "yjaques@gmail.com", ImmutableSet.<String>of("*")),

                        // the path where users are stored
                        Paths.get(getClass().getClassLoader().getResource("users.json").toURI()),

                        // the path where credentials are stored. isolating both is a good practice in terms of security
                        // it is strongly recommended to follow this approach even if you use your own repository
                        //NOT USING THIS ACTUALLY
                        Paths.get(getClass().getClassLoader().getResource("credentials.json").toURI()),

                        // tells that we want to reload the files dynamically if they are touched.
                        // this has a performance impact, if you know your users / credentials never change without a
                        // restart you can disable this to get better perfs
                        true),
                credentialsStrategy, defaultAdminPasswordHash),
                securitySettings);
    }

    @Provides @Named("restx.activation::restx.security.CORSFilter::CORSFilter")
    public String disableCorsFilter() {
        return "false";
    }

    //this allows CORS requests, I think we can disable if the Angular front-end code is hosted on the same server?
    @Provides
    public RestxFilter getCorsAuthorizerFilter() {
        return new RestxFilter() {
            @Override
            public Optional<RestxHandlerMatch> match(RestxRequest r) {
                return RestxHandlerMatch.of(Optional.of(new StdRestxRequestMatch(r.getRestxPath())),
                        new RestxHandler() {
                            @Override
                            public void handle(RestxRequestMatch match, RestxRequest req, RestxResponse resp, RestxContext ctx) throws IOException {
                                Optional<String> origin = req.getHeader("Origin");
                                if (origin.isPresent()) {
                                    resp.setHeader("Access-Control-Allow-Origin", origin.get());
                                    resp.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
                                    resp.setHeader("Access-Control-Allow-Credentials", Boolean.TRUE.toString());
                                    resp.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");

                                    if ("OPTIONS".equals(req.getHttpMethod())) {
                                        resp.setStatus(HttpStatus.OK);
                                    } else {
                                        ctx.nextHandlerMatch().handle(req, resp, ctx);
                                    }
                                } else {
                                    ctx.nextHandlerMatch().handle(req, resp, ctx);
                                }
                            }
                        }
                );
            }
        };
    }
}
