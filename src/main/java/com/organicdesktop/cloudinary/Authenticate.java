package com.organicdesktop.cloudinary;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.api.services.oauth2.Oauth2;
import com.google.api.services.oauth2.model.Userinfoplus;
import com.google.common.collect.ImmutableSet;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.organicdesktop.cloudinary.domain.RestUser;
import restx.security.RestxSession;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * This class holds the Google authentication information for a user, and compares to the internally defined set of roles.
 * If they match it creates the user in the session and this is used to determine access to rest services
 * the Angular front-end automatically calls the authentication service when the user logs in
 */
public class Authenticate {
    Logger logger = LoggerFactory.getLogger("Authenticate");
    private String DOMAIN ="@gmail.com";

    /**
     * used to check whether a user has a certain role
     *
     * @param token hash code given by the wannabe user
     * @return whether the user has that role
     */
    public RestUser authenticateUser(String token) {

        //get the list of defined rest users
        ArrayList<RestUser> restUsers = getRestUsers();

        //get the current requesting user info
        Userinfoplus user = getGoogleUser(token);

        if (user != null) {
            //if user.getEmail matches a rest users email and the role matches for that email then authenticate the user and set them as a RestxPrincipal
            for (int i = 0; i < restUsers.size(); i++) {
                //get the list of
                RestUser restUser = restUsers.get(i);

                if (user.getEmail().equals((restUser.getEmail()))) {
                    //email matches, so authenticate
                    RestxSession.current().authenticateAs(restUser);
                    logger.info("Authenticated user is: " + restUser.getName()  + restUser.getPrincipalRoles().toString());
                    return restUser;
                }
            }
            //we didn't match any specific user, but if they at least have a DOMAIN email we will make them a basic uploader
            if(user.getEmail().endsWith(DOMAIN)) {
                //make sure there is a user for this in the roles.json
                RestUser restUser = new RestUser("ORG User", "anon"+DOMAIN, ImmutableSet.<String>of(Roles.UPLOADER_ROLE));
                //email matches, so authenticate
                RestxSession.current().authenticateAs(restUser);
                logger.info("Authenticated user is: " + restUser.getName() + restUser.getPrincipalRoles().toString());
                return restUser;
            }
        }
        return null;
    }

    /**
     * used to check whether a user has a certain role
     * this isn't really needed anymore now that we are using BasicPrincipalAuthenticator
     *
     * @param roleType roleType to check
     * @param token    hashcode given by the wannabe user
     * @return whether the user has that role
     */
    public boolean verifyRole(String roleType, String token) {

        //authenticate user
        RestUser restUser = authenticateUser(token);

        if (restUser != null) {
            logger.info("Cool, we authenticated the user: " + restUser.getName());
            //now let's look at the roles although I think we can do this with the annotations now!!!!
            //so really we would just authenticate in the app module and that should do it
            Collection roles = restUser.getRoles();
            Iterator<String> str = roles.iterator();
            while(str.hasNext()) {
                if (roleType.equals(str.next())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * returns an ArrayList of users and their roles
     *
     * @return list of users and their roles
     */
    public ArrayList getRestUsers() {
        try (BufferedReader reader = new BufferedReader(new FileReader(getClass().getClassLoader().getResource("users.json").getFile()))) {
            return new Gson().fromJson(reader, new TypeToken<ArrayList<RestUser>>() {
            }.getType());
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    /**
     * uses the token received from the request and uses it to ask google for the user profile
     *
     * @return google user profile
     */
    public Userinfoplus getGoogleUser(String token) {
        try {
            GoogleCredential credential = new GoogleCredential().setAccessToken(token);
            Oauth2 oauth2 = new Oauth2.Builder(new NetHttpTransport(), new JacksonFactory(), credential).setApplicationName(
                    "Oauth2").build();
            return oauth2.userinfo().get().execute();
        } catch (IOException e) {
            logger.error(e.getMessage());
            return null;
        }
    }
}
