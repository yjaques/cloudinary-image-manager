package com.organicdesktop.cloudinary.rest;

import com.google.common.base.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.organicdesktop.cloudinary.Authenticate;
import com.organicdesktop.cloudinary.domain.RestUser;
import restx.annotations.GET;
import restx.annotations.RestxResource;
import restx.factory.Component;
import restx.security.PermitAll;
import restx.security.RestxSession;


@Component
@RestxResource
public class RestUserResource {

    Logger logger = LoggerFactory.getLogger("RestUserResource");
    /**
     * Authenticate the user.
     * The user sends the token they received from Google and the service checks for a match with its users.json table
     * This is currently a flat file but could easily come from any DB
     *
     * @return success
     */
    @GET("/authenticate/{authToken}")
    @PermitAll
    public String authenticate(String authToken) {

        Authenticate authenticate = new Authenticate();
        if(authenticate.authenticateUser(authToken) != null)
        {
            Optional<RestUser> principal = (Optional<RestUser>) RestxSession.current().getPrincipal();

            if(principal.isPresent()) {
                logger.info("principal is: " + principal.get().getName());
                return "{\"status\": \"success\"," +
                        "\"data\": " + principal.get().toJson() + "}";
            } else {
                logger.info("No principal found!");
                return "{\"status\": \"error\"," +
                        "\"data\": \"Principal not found.\"}";
            }

        } else {
            logger.warn("User not found or does not have sufficient rights");
            return "{\"status\": \"error\"," +
                    "\"data\": \"User not found in table\"}";
        }
    }
}