package com.organicdesktop.cloudinary.rest;

import com.cloudinary.Api;
import com.cloudinary.Cloudinary;
import com.cloudinary.api.ApiResponse;
import com.cloudinary.utils.ObjectUtils;
import com.google.common.base.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.organicdesktop.cloudinary.Roles;
import com.organicdesktop.cloudinary.domain.Custom;
import com.organicdesktop.cloudinary.domain.Image;
import com.organicdesktop.cloudinary.domain.RestUser;
import restx.annotations.GET;
import restx.annotations.POST;
import restx.annotations.RestxResource;
import restx.factory.Component;
import restx.security.PermitAll;
import restx.security.RestxSession;
import restx.security.RolesAllowed;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.*;


@Component
@RestxResource
public class ImageResource {

    Logger logger = LoggerFactory.getLogger("ImageResource");

    //connect to cloudinary
    private Map config = ObjectUtils.asMap(
            "cloud_name", "your cloud name here",
            "api_key", "your api_key here",
            "api_secret", "your api_secret here"
    );

    private Cloudinary cloudinary = new Cloudinary(config);
    private Api api = cloudinary.api();
    private ArrayList<String> baseParams = new ArrayList<>(Arrays.asList(
            "max_results", "50",
            "tags", "true",
            "context", "true",
            "moderations", "true"
    ));

    /**
     * list all images.
     *
     * @return JSON list of ALL images in repository
     */
    @RolesAllowed(Roles.UPLOADER_ROLE)
    @GET("/list_images")
    public ApiResponse listImages(Optional<String> nextCursor) {
        try {

            ArrayList<String> params = baseParams;
            if (nextCursor.isPresent()) {
                params.add("next_cursor");
                params.add(nextCursor.get());
            }
            return api.resources(ObjectUtils.asMap(params.toArray()));

        } catch (Exception e) {
            logger.error(e.toString());
            return null;
        }
    }

    /**
     * List all images that have been uploaded (i.e. not transforms)
     * not sure this service is very useful!
     *
     * @return JSON list of images
     */
    @RolesAllowed(Roles.UPLOADER_ROLE)
    @GET("/list_uploaded_images")
    public ApiResponse listUploadedImages(Optional<String> nextCursor) {
        try {
            ArrayList<String> params = baseParams;
            if (nextCursor.isPresent()) {
                params.add("next_cursor");
                params.add(nextCursor.get());
            }
            params.add("type");
            params.add("upload");
            return api.resources(ObjectUtils.asMap(params.toArray()));
        } catch (Exception e) {
            logger.error(e.toString());
            return null;
        }
    }

    /**
     * List images by tag
     *
     * @return JSON list of images
     */
    @RolesAllowed(Roles.UPLOADER_ROLE)
    @GET("/list_images_by_tag/{tag}")
    public ApiResponse listImagesByTag(String tag, Optional<String> nextCursor) {
        try {
            ArrayList<String> params = baseParams;
            if (nextCursor.isPresent()) {
                params.add("next_cursor");
                params.add(nextCursor.get());
            } else {
                //who knows why but if you are using next_cursor and then stop using it cloudinary seems to keep handing you the same set of resources back.
                //not sure if it's a bug on their side or something strange that I'm doing.
                params.add("next_cursor");
                params.add("");
            }

            return api.resourcesByTag(tag, ObjectUtils.asMap(params.toArray()));
        } catch (Exception e) {
            logger.error(e.toString());
            return null;
        }
    }

    /**
     * List public images, no role required
     *
     * @return JSON list of images
     */
    @GET("/list_public_images")
    @PermitAll
    public ApiResponse listPublicImages(Optional<String> nextCursor) {
        try {
            ArrayList<String> params = baseParams;
            if (nextCursor.isPresent()) {
                params.add("next_cursor");
                params.add(nextCursor.get());
            } else {
                //who knows why but if you are using next_cursor and then stop using it cloudinary seems to keep handing you the same set of resources back.
                //not sure if it's a bug on their side or something strange that I'm doing.
                params.add("next_cursor");
                params.add("");
            }

            return api.resourcesByTag("public", ObjectUtils.asMap(params.toArray()));
        } catch (Exception e) {
            logger.error(e.toString());
            return null;
        }
    }

    /**
     * List images by start date
     *
     * @return JSON list of images
     */
    @RolesAllowed(Roles.UPLOADER_ROLE)
    @GET("/list_images_by_date/{date}")
    public ApiResponse listImagesByDate(String date, Optional<String> nextCursor) {
        try {
            ArrayList<String> params = baseParams;
            params.add("start_at");
            params.add(date);

            if (nextCursor.isPresent()) {
                params.add("next_cursor");
                params.add(nextCursor.get());
            }
            return api.resources(ObjectUtils.asMap(params.toArray()));
        } catch (Exception e) {
            logger.error(e.toString());
            return null;
        }
    }

    /**
     * List images that have been uploaded (i.e. not transforms)
     *
     * @return JSON list of images
     */
    @RolesAllowed(Roles.EDITOR_ROLE)
    @GET("/list_images_to_be_moderated")
    public ApiResponse listImagesToBeModerated(Optional<String> nextCursor) {

        try {
            ArrayList<String> params = baseParams;

            //just load 50 images instead of default 100
            //params.set(1, "50");

            if (nextCursor.isPresent()) {
                params.add("next_cursor");
                params.add(nextCursor.get());
            }
            return api.resourcesByModeration("manual", "pending", ObjectUtils.asMap(params.toArray()));
        } catch (Exception e) {
            logger.error(e.toString());
            return null;
        }
    }

    /**
     * List all tags (up to 500) -- if more are needed implement next_cursor
     *
     * @return JSON list of tags
     */
    @RolesAllowed(Roles.UPLOADER_ROLE)
    @GET("/list_tags")
    public ApiResponse listTags() {
        try {
            return api.tags(ObjectUtils.asMap("max_results", 500));
        } catch (Exception e) {
            logger.error(e.toString());
            return null;
        }
    }

    /**
     * Update tags or context metadata of an image
     *
     * @return success
     */
    @RolesAllowed(Roles.UPLOADER_ROLE)
    @POST("/update_image/{id}")
    public ApiResponse updateImage(String id, Image image) {
        logger.info("image tags look like: " + image.getTags());
        logger.info("image context looks like: " + image.getContext().getCustom().toString());

        ArrayList<String> params = new ArrayList<>();

        //tags should be comma-separated
        List tags = image.getTags();
        StringBuffer tagStr = new StringBuffer();
        for (int i = 0; i < tags.size(); i++) {
            tagStr.append(tags.get(i));
            if (i < tags.size() - 1) {
                tagStr.append(",");
            }
        }
        params.add("tags");
        params.add(tagStr.toString());

        //context should be pipe | separated
        Custom custom = image.getContext().getCustom();

        StringBuffer contextStr = new StringBuffer();

        contextStr.append("title=").append(custom.getTitle())
                .append("|caption=").append(custom.getCaption())
                .append("|location=").append(custom.getLocation())
                .append("|countryCode=").append(custom.getCountryCode())
                .append("|copyright=").append(custom.getCopyright())
                .append("|date=").append(custom.getDate())
                .append("|credit=").append(custom.getCredit())
                .append("|lat=").append(custom.getLat())
                .append("|lng=").append(custom.getLng())
                .append("|keywords=").append(custom.getKeywords())
                .append("|printQuality=").append(custom.getPrintQuality())
                .append("|theme=").append(custom.getTheme());

        params.add("context");
        params.add(contextStr.toString());

        try {
            return api.update(id, ObjectUtils.asMap(params.toArray()));
        } catch (Exception e) {
            logger.error(e.toString());
            return null;
        }
    }

    /**
     * Approve or Reject an image in the moderation queue
     * status is either approved or rejected
     *
     * @return success
     */
    @RolesAllowed(Roles.EDITOR_ROLE)
    @GET("/moderate_image/{id}")
    public ApiResponse moderateImage(String id, String status, Optional<String> tags) throws IOException {
        String decodedId = URLDecoder.decode(id, "UTF-8");

        ArrayList<String> params = new ArrayList<>();

        params.add("moderation_status");
        params.add(status);

        if (tags.isPresent()) {
            params.add("tags");
            params.add(tags.get());
        }

        try {
            return api.update(decodedId, ObjectUtils.asMap(params.toArray()));
        } catch (Exception e) {
            logger.error(e.toString());
            return null;
        }
    }

    /**
     * Delete an image in the library
     *
     * @return success
     */
    @RolesAllowed(Roles.UPLOADER_ROLE)
    @GET("/delete_image/{id}")
    public ApiResponse deleteImage(String id) throws IOException {

        String decodedId = URLDecoder.decode(id, "UTF-8");

        //make sure that if user is uploader they are only deleting their own pending image
        //get user
        Optional<RestUser> principal = (Optional<RestUser>) RestxSession.current().getPrincipal();

        //check if editor
        Iterator<String> iter = principal.get().getPrincipalRoles().iterator();
        while (iter.hasNext()) {
            //if they are an editor they can do what they want
            if (iter.next().equals(Roles.EDITOR_ROLE)) {
                try {
                    return api.deleteResources(Arrays.asList(decodedId), ObjectUtils.emptyMap());
                } catch (Exception e) {
                    logger.error(e.toString());
                    return null;
                }
            }
        }
        try {
            //not an editor so get the image metadata to check it against the user
            ApiResponse resource = api.resource(decodedId, ObjectUtils.emptyMap());
            //iterate looking for the "moderation" entry and see if it is pending
            Collection imageValues = resource.entrySet();
            Iterator vals = imageValues.iterator();
            while (vals.hasNext()) {
                Map.Entry pair = (Map.Entry) vals.next();
                //make sure the file is still in moderation queue
                if (pair.getKey().equals("moderation") && !pair.getValue().toString().contains("pending")) {
                    logger.error("Image is not in moderation queue");
                    return null;
                }
            }
            //iterate looking for the "tags" entry and see if there is one that matches the user's email
            vals = imageValues.iterator();
            while (vals.hasNext()) {
                Map.Entry pair = (Map.Entry) vals.next();
                if (pair.getKey().equals("tags")) {
                    ArrayList tags = (ArrayList) pair.getValue();
                    Iterator tagIt = tags.iterator();
                    while (tagIt.hasNext()) {
                        if (tagIt.next().equals(principal.get().getEmail())) {
                            logger.info("User matches image and may delete");
                            try {
                                return api.deleteResources(Arrays.asList(decodedId), ObjectUtils.emptyMap());
                            } catch (Exception e) {
                                logger.error(e.toString());
                                return null;
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            logger.error(e.toString());
        }
        try {
            return api.deleteResources(Arrays.asList(decodedId), ObjectUtils.emptyMap());
        } catch (Exception e) {
            logger.error(e.toString());
            return null;
        }
    }

    /**
     * Delete an image in the moderation queue
     *
     * @return success
     */
    @RolesAllowed(Roles.EDITOR_ROLE)
    @GET("/delete_images_by_tag/{tag}")
    public ApiResponse deleteImagesByTag(String tag) throws IOException {

        try {
            return api.deleteResourcesByTag(tag, ObjectUtils.emptyMap());
        } catch (Exception e) {
            logger.error(e.toString());
            return null;
        }

    }
}