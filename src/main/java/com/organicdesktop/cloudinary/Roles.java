package com.organicdesktop.cloudinary;

/**
 * A list of roles for the application.
 *
 * We don't use an enum here because it must be used inside an annotation.
 */
public final class Roles {
    public static final String UPLOADER_ROLE = "uploader";
    public static final String EDITOR_ROLE = "editor";
    public static final String ADMIN_ROLE = "restx-admin";

    private Roles(){
        //this prevents even the native class from calling this constructor:
        throw new AssertionError();
    }
}
