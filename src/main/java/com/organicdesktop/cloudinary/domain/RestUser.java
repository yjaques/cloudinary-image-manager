package com.organicdesktop.cloudinary.domain;

import com.google.common.collect.ImmutableSet;
import restx.security.RestxPrincipal;

import java.util.Collection;
import java.util.Iterator;

/**
 * a defined user within the system
 */
public class RestUser implements RestxPrincipal {
    private String name;
    private String email;
    private Collection<String> roles;

    public RestUser(String name, String email, Collection<String> roles) {
        this.name = name;
        this.email = email;
        this.roles = roles;
    }

    //dummy constructor for Jackson otherwise it will not convert the users.json into java instances
    public RestUser() {
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Collection getRoles() {
        return roles;
    }

    public void setRoles(Collection<String> roles) {
        this.roles = roles;
    }

    @Override
    public ImmutableSet<String> getPrincipalRoles() {
        return ImmutableSet.copyOf(roles);
    }

    @Override
    public String toString() {
        return "name=" + name + "email=" + email + "roles=" + roles.toString();
    }

    public String toJson() {
        StringBuilder roleJson = new StringBuilder();
        Iterator<String> iter = roles.iterator();
        while(iter.hasNext()) {
            roleJson.append("\"").append(iter.next()).append("\"");
            if(iter.hasNext()) {
                roleJson.append(",");
            }
        }

        return "{\"name\": \"" + name + "\"," +
                "\"email\": \"" + email + "\"," +
                "\"roles\": [" + roleJson + "]}";
    }
}
