package com.organicdesktop.cloudinary.domain;

import java.util.List;

public class Image {
    private List tags;
    private Context context;

    //dummy constructor for Jackson
    public Image() {
    }

    public Image setTags(final List tags) {
        this.tags = tags;
        return this;
    }

    public List getTags() {
        return tags;
    }

    public Image setContext(final Context context) {
        this.context = context;
        return this;
    }

    public Context getContext() {
        return context;
    }

}
