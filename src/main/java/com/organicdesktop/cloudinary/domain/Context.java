package com.organicdesktop.cloudinary.domain;

public class Context {
    private Custom custom;

    public Context setCustom(final Custom custom) {
        this.custom = custom;
        return this;
    }

    public Custom getCustom() {
        return custom;
    }
}