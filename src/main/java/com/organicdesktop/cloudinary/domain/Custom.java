package com.organicdesktop.cloudinary.domain;

public class Custom {
    private String title;
    private String caption;
    private String location;
    private String copyright;
    private String date;
    private String credit;
    private String lat;
    private String lng;
    private String keywords;
    private String printQuality;
    private String countryCode;
    private String theme;


    public Custom setTitle(final String title) {
        this.title = title;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public Custom setCaption(final String caption) {
        this.caption = caption;
        return this;
    }

    public String getCaption() {
        return caption;
    }

    public Custom setLocation(final String location) {
        this.location = location;
        return this;
    }

    public String getLocation() {
        return location;
    }

    public Custom setCopyright(final String copyright) {
        this.copyright = copyright;
        return this;
    }

    public String getCopyright() {
        return copyright;
    }
    
    public Custom setDate(final String date) {
        this.date = date;
        return this;
    }

    public String getDate() {
        return date;
    }
    
    public Custom setCredit(final String credit) {
        this.credit = credit;
        return this;
    }

    public String getCredit() {
        return credit;
    }

    public Custom setLat(final String lat) {
        this.lat = lat;
        return this;
    }

    public String getLat() {
        return lat;
    }

    public Custom setLng(final String lng) {
        this.lng = lng;
        return this;
    }

    public String getLng() {
        return lng;
    }

    public Custom setKeywords(final String keywords) {
        this.keywords = keywords;
        return this;
    }

    public String getKeywords() {
        return keywords;
    }

    public Custom setPrintQuality(final String printQuality) {
        this.printQuality = printQuality;
        return this;
    }

    public String getPrintQuality() {
        return printQuality;
    }

    public Custom setCountryCode(final String countryCode) {
        this.countryCode = countryCode;
        return this;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public Custom setTheme(final String theme) {
        this.theme = theme;
        return this;
    }

    public String getTheme() {
        return theme;
    }

    @Override
    public String toString() {
        return title+caption+location+copyright+date+credit+lat+lng+keywords;
    }

}
