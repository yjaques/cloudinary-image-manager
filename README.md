# README #

## What is this repository for? ##

### Quick summary: ###
This repository contains a set of services for managing images with Cloudinary SaaS:

1. A set of Java RestX framework services that interact with the Cloudinary API and 
2. an AngularJS / Bootstrap responsive design front-end that interacts with the RestX services and Google OAUTH to give you a basic image management workflow.

It can be implemented over any instance of the Cloudinary image management SaaS cloud solution.

## How do I get set up? ##

###Summary of set up ###
There's a POM so you should be able to pull the GIT repo into any Java IDE without much trouble.

### Configuration ###
You will need to start by going to Cloudinary and set up a free account and then change the config in the ImageResource class to point to your instance:

```
#!java
    private Map config = ObjectUtils.asMap(
            "cloud_name", "your_cloud_name",
            "api_key", "your_api_key",
            "api_secret", "your_api_secret"
    );

```
Secondly, note that you must create an "upload preset" in Cloudinary in order to enable the right workflow. Your preset should have the following set of configurations:


```
#!txt
	
Unsigned: yes
Use filename: false
Disallow public ID: true  
Allowed formats: jpg,jpeg,png,gif  
Type: upload  
Image metadata: true  
Format: jpg  
Moderation: manual  
Return delete token: true

```

Once you have created your preset, you will need to add its ID to the /src/main/webapp/scripts/config.js as well as your cloud ID:
```
#!js
//this is here on its own as it's used by the jquery based uploader
$.cloudinary.config().cloud_name = 'your_cloud_id';
//upload presets are set in the system and control a number of factors of how the image is uploaded
$.cloudinary.config().upload_preset = 'your_upload_preset';
```

### User Configuration ###
It is designed to use Google's OAUTH2 mechanism for authentication. You could switch to another but it would be some work as the mechanism is:

1. You login through Google which returns user details and a token.
2. Any action sends along the token and RestX checks with Google to make sure the action is actually coming from the logged in user and
3. RestX authenticates that user against the role to make sure they have the right to do that action.

RestX is very flexible in how it handles authentication. For simplicity, I created a JSON file users.json in the src/main/resources folder in which you can put in user names, emails and roles. You could also easily implement that as a database, e.g. MongoDB that returns JSON in the same format or write a bit of code to hook up any existing authentication service you want.

There is a default domain user as well that you can set in the JSON file to simply have upload privileges. As long as their email is in the domain they can upload. It simply checks the logged user against the list to see if the action is permitted for the uploader role.

Roles: note that roles are arbitrary. You define them in the Roles class and then use them as annotations for any rest action like:
```
#!java
   @RolesAllowed(Roles.UPLOADER_ROLE)
   @GET("/list_images")
   public ApiResponse listImages(Optional<String> nextCursor) {
       //some code to interact with cloudinary
   }
```
Indeed the real workhorse is the ImageResource class, that has all the rest services that interact with cloudinary. Take a look at that.

Note that in the AppModule class there is a Restx-admin defined with its own password. This is purely to access a web interface that lets you easily browse and test the rest services. To see the admin console just browse to http://yourserver:yourport/yourContextIfAny/api/@/ui/

### Deployment instructions ###

Unless you're running on localhost:8080/cloudinary as I am, you will need to set up your own OAUTH clientID and enable some APIs in the Google Developer Console. This is pretty easy. Just go to https://console.developers.google.com, create a new project, go to Credentials and set up an OAuth Client ID and fill in the OAUTH Consent Screen. Once generated you will need to go in and update the /src/main/webapp/scripts/app.js file CLIENT_ID constant:

```
#!js
app.constant('CONSTANTS', {
    ClIENT_ID: 'yourClientID',
    GOOGLE_PROFILE_URL: 'https://www.googleapis.com/userinfo/v2/me',
    GOOGLE_API_KEY: 'Google Geo Locate API Key, not using it but you could, code is there, just sign up for a key',
    DOMAIN: 'gmail.com',
    PROD: false,
    GEO_LOCATE: true,
    FLASH_TIME: 5000,
    MAX_FILES: 10,
    PERMA_TAGS: 'org,public'
});

```

Note there are a couple other interesting constants here. Set PROD to true when in production so your images will be tagged as production instead of dev. You can also disable geoLocation, set the flash time for messages, decide how many files to let people upload and what tags are not able to be removed from an image (best leave that one alone).

Now, go back into the Google developer's console and also enable the Google+ API so that you can read the logged user's details, i.e. name, email.

Finally, in /src/main/webapp/data/themes.json you can set your categories for your own taxonomy.

### How to run tests ###
NG Mock is there but tests have yet to be written, ;-((

## TO DO ##
1. Write tests. Would be great to get some set up, would like to see mocking the Google Web API responses.
2. API Documenation.